package roman;

import static org.junit.Assert.*;
import org.junit.Test;

import java.util.regex.Pattern;

import roman.RomanConverter;

public class test {
	private RomanConverter romanconverter = new RomanConverter();
	

	@Test
	public void test1(){
			assertEquals(1000, romanconverter.fromRoman("M"));
			assertEquals("M", romanconverter.toRoman(1000));
			assertEquals(2018, romanconverter.fromRoman("MMXVIII"));
			assertEquals("MMXVIII", romanconverter.toRoman(2018));
	}
	@Test(expected = Exception.class)
	public void test2(){
					romanconverter.toRoman(5000);
					}
					

	@Test(expected = Exception.class)
	public void test3(){
					romanconverter.fromRoman("dsfv");

	}
	
	@Test(expected = Exception.class)
	public void test4(){
					romanconverter.toRoman(-5000);
					}
}